#include <stdio.h>

extern void print_a();
extern void print_b();

int main(int argc, char *argv[])
{
    (void)(argc);
    (void)(argv);

    print_a();
    print_b();

    return 0;
}
