cmake_minimum_required(VERSION 3.5)
project (qgs)

add_compile_options("-Wall" "-W" $<$<CONFIG:Debug>:-Werror>)
set(CMAKE_CXX_STANDARD 11)
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/)
include(pch)

add_executable(main main.cpp a.cpp b.cpp)
add_precompile_header(main "preinclude.hpp")
